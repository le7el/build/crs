// SPDX-License-Identifier: BSD-2-Clause
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/utils/Address.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "../lib/StringUtils.sol";
import "../nft/IBaseNFT.sol";
import "../resolver/IResolver.sol";
import "../registry/ICRS.sol";
import "../resolver/profile/IProxyConfigResolver.sol";

/**
 * @dev A registrar controller for registering and renewing names at fixed cost in gas coin.
 */
contract Controller is Ownable {
    using StringUtils for *;
    using SafeERC20 for IERC20;
    using Address for address payable;
    using Address for address;

    uint256 constant public MIN_REGISTRATION_DURATION = 28 days;
    uint256 constant public MIN_COMMITMENT_AGE = 60;
    uint256 constant public MAX_COMMITMENT_AGE = 86400;

    bytes4 constant public TOKEN_URI_SELECTOR = bytes4(keccak256("tokenURI(string)"));
    bytes4 constant private INTERFACE_META_ID = bytes4(keccak256("supportsInterface(bytes4)"));
    bytes4 constant private COMMITMENT_CONTROLLER_ID = bytes4(
        keccak256("rentPrice(string,uint256)") ^
        keccak256("available(string)") ^
        keccak256("makeCommitment(string,address,bytes32)") ^
        keccak256("commit(bytes32)") ^
        keccak256("register(string,address,uint256,bytes32)") ^
        keccak256("renew(string,uint256)")
    );

    bytes4 constant private COMMITMENT_WITH_CONFIG_CONTROLLER_ID = bytes4(
        keccak256("registerWithConfig(string,address,uint256,bytes32,address,address)") ^
        keccak256("makeCommitmentWithConfig(string,address,bytes32,address,address)")
    );

    IBaseNFT public immutable base;
    ICRS public immutable crs;
    bytes32 public immutable baseNode;

    mapping(bytes32=>uint) public commitments;
    uint256 public maxLeasePeriod;

    event NameRegistered(string name, bytes32 indexed label, address indexed owner, address indexed token, uint cost, uint expires);
    event NameRenewed(string name, bytes32 indexed label, address indexed token, uint cost, uint expires);
    event PaymentReceived(address indexed payer, uint256 amount);
    event MaxLeaseChanged(address indexed owner, uint256 duration);

    /**
     * @dev Set NFT smart contract, price oracle and default settings.
     * @param _base Address of NFT smart contract.
     */
    constructor(IBaseNFT _base) Ownable() {
        base = _base;
        crs = _base.crs();
        baseNode = _base.baseNode();
    }

    /**
     * @dev Returns the price to register or renew a name.
     * @param _duration How long the name is being registered or extended for, in seconds.
     * @return The ERC20 token address (0 for gas token) and the price of this renewal.
     */
    function rentPrice(string memory _name, uint _duration) public view returns(address, uint) {
        bytes32 _baseNode = baseNode;
        (, uint256 _amount, address _token) = IResolver(crs.resolver(_baseNode)).royalty(_baseNode, address(this));
        
        uint256 _len = _name.strlen();
        if (_len == 4) {
            return (_token, _amount * _duration * 5);
        } else if (_len == 3) {
            return (_token, _amount * _duration * 10);
        } else if (_len == 2) {
            return (_token, _amount * _duration * 50);
        } else if (_len == 1) {
            return (_token, _amount * _duration * 250);
        }
        return (_token, _amount * _duration);
    }

    /**
     * @dev Check if name is longer than 1 symbol.
     * @param _name The name being registered or renewed.
     * @return true if valid.
     */
    function valid(string memory _name) public pure returns(bool) {
        return _name.strlen() >= 1;
    }

    /**
     * @dev Check if name is not registered already.
     * @param _name The name being registered or renewed.
     * @return true if available.
     */
    function available(string memory _name) public view returns(bool) {
        bytes32 _label = keccak256(bytes(_name));
        return valid(_name) && base.available(uint256(_label));
    }

    /**
     * @dev Ticket to reserve registration spot.
     * @param _name The name being registered.
     * @param _owner Address which will own the name.
     * @param _secret Secret to confirm commitment.
     * @return node hash of a new record.
     */
    function makeCommitment(string memory _name, address _owner, bytes32 _secret) public pure returns(bytes32) {
        return makeCommitmentWithConfig(_name, _owner, _secret, address(0), address(0));
    }

    /**
     * @dev Ticket to reserve registration spot with default resolver and address.
     * @param _name The name being registered.
     * @param _owner Address which will own the name.
     * @param _secret Secret to confirm ownership.
     * @param _resolver Resolver smart contract to store record data.
     * @param _addr Reverse record for an address.
     * @return node hash of a new record.
     */
    function makeCommitmentWithConfig(
        string memory _name,
        address _owner,
        bytes32 _secret,
        address _resolver,
        address _addr
    ) public pure returns(bytes32) {
        bytes32 _label = keccak256(bytes(_name));
        if (_resolver == address(0) && _addr == address(0)) {
            return keccak256(abi.encodePacked(_label, _owner, _secret));
        }
        require(_resolver != address(0), "resolver forbidden");
        return keccak256(abi.encodePacked(_label, _owner, _resolver, _addr, _secret));
    }

    /**
     * @dev Start registration process.
     * @param _commitment Reservation ticket.
     */
    function commit(bytes32 _commitment) public virtual {
        // solhint-disable not-rely-on-time
        require(commitments[_commitment] + MAX_COMMITMENT_AGE < block.timestamp, "expired commitment");
        // solhint-disable not-rely-on-time
        commitments[_commitment] = block.timestamp;
    }

    /**
     * @dev Finish registration process.
     * @param _name The name being registered.
     * @param _owner Address which will own the name.
     * @param _duration Seconds lease duration.
     * @param _secret Secret to confirm commitment.
     */
    function register(string calldata _name, address _owner, uint _duration, bytes32 _secret) external payable {
        registerWithConfig(_name, _owner, _duration, _secret, address(0), address(0));
    }

    /**
     * @dev Finish registration process, setting the resolver and address.
     * @param _name The name being registered.
     * @param _owner Address which will own the name.
     * @param _duration Lease duration in seconds.
     * @param _secret Secret to confirm commitment.
     * @param _resolver Resolver smart contract to store record data.
     * @param _addr Reverse record for an address.
     */
    function registerWithConfig(
        string memory _name,
        address _owner,
        uint _duration,
        bytes32 _secret,
        address _resolver,
        address _addr
    ) public payable {
        (address _paymentToken, uint _cost, uint _gasValue) = _consumeCommitment(
            _name,
            _duration,
            makeCommitmentWithConfig(_name, _owner, _secret, _resolver, _addr)
        );

        bytes32 _label = keccak256(bytes(_name));
        uint256 _tokenId = uint256(_label);

        // Store name on-chain to generate metadata.
        base.saveName(_name);

        uint _expires;
        if(_resolver != address(0)) {
            // Set this contract as the (temporary) owner, giving it
            // permission to set up the resolver.
            _expires = _doRegistration(_tokenId, _duration, address(this), _gasValue);

            // The nodehash of this label
            bytes32 _nodehash = keccak256(abi.encodePacked(base.baseNode(), _label));

            // Set the resolver
            crs.setResolver(_nodehash, _resolver);

            // Configure the resolver
            if (_addr != address(0)) {
                IResolver(_resolver).setAddr(_nodehash, _addr);
            }

            // Now transfer full ownership to the expected owner
            base.reclaim(_tokenId, _owner);
            base.transferFrom(address(this), _owner, _tokenId);
        } else {
            require(_addr == address(0), "no reverse addr allowed");
            _expires = _doRegistration(_tokenId, _duration, _owner, _gasValue);
        }

        emit NameRegistered(_name, _label, _owner, _paymentToken, _cost, _expires);
    }

    /**
     * @dev Extend the lease of previously registered name.
     * @param _name The name being registered.
     * @param _duration Lease duration in seconds.
     */
    function renew(string calldata _name, uint _duration) external payable {
        bytes32 _label = keccak256(bytes(_name));
        uint256 _maxLeasePeriod = maxLeasePeriod;
        // solhint-disable not-rely-on-time
        require(_maxLeasePeriod == 0 || base.nameExpires(uint256(_label)) + _duration <= block.timestamp + maxLeasePeriod, "too long lease");
        
        (address _paymentToken, uint _cost) = rentPrice(_name, _duration);
        uint256 _gasValue = msg.value;
        if (_cost > 0) {
            if (_paymentToken == address(0)) {
                require(msg.value >= _cost, "payment failed");
                _gasValue -= _cost;
            } else {
                IERC20(_paymentToken).safeTransferFrom(msg.sender, address(this), _cost);
            }
        }

        uint _expires;
        if (_gasValue == 0) {
            _expires = base.renew(uint256(_label), _duration);
        } else {
            (_expires) = abi.decode(address(base).functionCallWithValue(
                abi.encodeWithSignature("renew(uint256,uint256)", uint256(_label), _duration),
                _gasValue
            ), (uint256));
        }

        emit NameRenewed(_name, _label, _paymentToken, _cost, _expires);
    }

    /**
     * @dev Withdraw the fees in ERC20 or gas token.
     * @param _paymentToken ERC20 token address for withdrawal, address(0) for gas coin
     */
    function withdraw(address _paymentToken) external onlyOwner {
        if (_paymentToken == address(0)) {
            payable(msg.sender).sendValue(address(this).balance);     
        } else {
            address _me = address(this);
            uint256 _balance = IERC20(_paymentToken).balanceOf(_me);
            IERC20(_paymentToken).safeTransfer(msg.sender, _balance);
        }  
    }

    /**
     * @dev Can recieve gas token payments.
     */
    receive() external payable {
        emit PaymentReceived(msg.sender, msg.value);
    }

    /**
     * @dev Allow limitation of maximum lease period for early projects.
     * @param _maxLeasePeriod Maximum lease period in seconds, pass 0 to disable.
     */
    function adminMaxLeasePeriod(uint256 _maxLeasePeriod) external onlyOwner {
        maxLeasePeriod = _maxLeasePeriod;
        emit MaxLeaseChanged(msg.sender, _maxLeasePeriod);
    }

    /**
     * @dev Check if specific interface is implemented.
     * @param _interfaceID Keccak of matched interface.
     * @return true if interface is implemented.
     */
    function supportsInterface(bytes4 _interfaceID) public virtual pure returns (bool) {
        return _interfaceID == INTERFACE_META_ID ||
               _interfaceID == COMMITMENT_CONTROLLER_ID ||
               _interfaceID == COMMITMENT_WITH_CONFIG_CONTROLLER_ID;
    }

    /**
     * @dev Use commitment to register commited name.
     * @param _name The name being registered.
     * @param _duration Lease duration in seconds.
     * @param _commitment Commitment hash.
     * @return registration cost in wei or ERC20 token, remaining gas coin.
     */
    function _consumeCommitment(
        string memory _name,
        uint _duration,
        bytes32 _commitment
    ) internal returns (address, uint256, uint256) {
        // Require a valid commitment, if the commitment is too old, or the name is registered, stop
        uint256 _ts = commitments[_commitment];
        // solhint-disable not-rely-on-time
        uint256 _bts = block.timestamp;
        require(_ts + MIN_COMMITMENT_AGE <= _bts, "await maturation");
        require(_ts + MAX_COMMITMENT_AGE > _bts, "expired commitment");
        require(available(_name), "not available");

        delete commitments[_commitment];

        require(_duration >= MIN_REGISTRATION_DURATION, "min 1 month");
        uint256 _maxLeasePeriod = maxLeasePeriod;
        require(_maxLeasePeriod == 0 || _duration <= _maxLeasePeriod, "too long lease");

        (address _tokenAddr, uint _cost) = rentPrice(_name, _duration);
        if (_cost > 0) {
            if (_tokenAddr == address(0)) {
                require(msg.value >= _cost, "payment failed");
                return (_tokenAddr, _cost, msg.value - _cost);
            } else if (_cost > 0) {
                IERC20(_tokenAddr).safeTransferFrom(msg.sender, address(this), _cost);
            }
        }
        return (_tokenAddr, _cost, msg.value);
    }

    /**
     * @dev Processed with registration.
     * @param _tokenId NFT id of CRS.
     * @param _duration Lease duration in seconds.
     * @param _owner Address to own NFT.
     * @param _gasValue Amount of gas coin to be paid.
     * @return expiration ts in seconds.
     */
    function _doRegistration(uint256 _tokenId, uint256 _duration, address _owner, uint256 _gasValue) internal returns(uint256) {
        if (_gasValue > 0) {
            (uint256 _expires) = abi.decode(payable(address(base)).functionCallWithValue(
                abi.encodeWithSignature("register(uint256,address,uint256)", _tokenId, _owner, _duration),
                _gasValue
            ), (uint256));
            return _expires;
        }
        return base.register(_tokenId, _owner, _duration);
    }
}