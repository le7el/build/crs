// SPDX-License-Identifier: BSD-2-Clause
pragma solidity ^0.8.0;

import "./Controller.sol";

/**
 * @dev Controller with whitelisting feature to protect namespace.
 */
contract GatedController is Controller {
    bytes4 constant private COMMITMENT_WITH_WHITELIST = bytes4(
        keccak256("commit(string,bytes32,bytes)") ^
        keccak256("isAllowed(address,string,bytes)") ^
        keccak256("getDomainSeparator()")
    );

    /// @dev Value returned by a call to `isAllowed` if the check
    /// was successful. The value is defined as:
    /// bytes4(keccak256("isAllowed(address,string,bytes)"))
    bytes4 internal constant MAGICVALUE = 0xe3500b28;

    /// @dev The EIP-712 domain type hash used for computing the domain
    /// separator.
    bytes32 internal constant DOMAIN_TYPE_HASH =
        keccak256(
            "EIP712Domain(string name,string version,uint256 chainId,address verifyingContract)"
        );

    /// @dev The EIP-712 domain name used for computing the domain separator.
    bytes32 internal constant DOMAIN_NAME = keccak256("IdentityWhitelist");

    /// @dev The EIP-712 domain version used for computing the domain separator.
    bytes32 internal constant DOMAIN_VERSION = keccak256("v1");

    /// @dev The domain separator used for signing orders that gets mixed in
    /// making signatures for different domains incompatible. This domain
    /// separator is computed following the EIP-712 standard and has replay
    /// protection mixed in so that signed orders are only valid for specific
    /// contracts.
    bytes32 public immutable DOMAIN_SEPARATOR;

    bool public whitelistDisabled;
    mapping(address => bool) public validators;

    event ValidatorAdded(address indexed validator);
    event ValidatorRemoved(address indexed validator);
    event WhitelistStatusChanged(address owner, bool indexed status);

    constructor(IBaseNFT _base) Controller(_base) {
        // NOTE: Currently, the only way to get the chain ID in solidity is
        // using assembly.
        uint256 chainId;
        // solhint-disable-next-line no-inline-assembly
        assembly {
            chainId := chainid()
        }
        
        DOMAIN_SEPARATOR = keccak256(
            abi.encode(
                DOMAIN_TYPE_HASH,
                DOMAIN_NAME,
                DOMAIN_VERSION,
                chainId,
                address(this)
            )
        );
    }

    /**
     * @dev Unpermissioned commitment is blocked while whitelist is active.
     *
     * @param _commitment Reservation ticket.
     */
    function commit(bytes32 _commitment) public override {
        require(whitelistDisabled, "use with whitelisting params");
        super.commit(_commitment);
    }

    /**
     * @dev Start registration process.
     *
     * @param _name Name for registration.
     * @param _commitment Reservation ticket.
     * @param _pass Whitelisting ticket.
     */
    function commit(string memory _name, bytes32 _commitment, bytes calldata _pass) public {
        require(isAllowed(msg.sender, _name, _pass) == MAGICVALUE, "not whitelisted");
        super.commit(_commitment);
    }

    /**
     * @dev Checks if user is whitelisted wallet.
     *
     * @param _user Address to check for whitelisting.
     * @param _name Whitelisted identity name.
     * @param _pass Digest of signed wallets.
     * @return 0xe3500b28 for success 0x00000000 for failure.
     */
    function isAllowed(
        address _user,
        string memory _name,
        bytes calldata _pass
    ) public view returns (bytes4) {
        uint8 _v;
        bytes32 _r;
        bytes32 _s;
        (_v, _r, _s) = abi.decode(_pass, (uint8, bytes32, bytes32));
        bytes32 _hash = keccak256(abi.encode(getDomainSeparator(), _user, _name));
        address _signer =
            ecrecover(
                keccak256(
                    abi.encodePacked("\x19Ethereum Signed Message:\n32", _hash)
                ),
                _v,
                _r,
                _s
            );

        if (validators[_signer]) return MAGICVALUE;
        return bytes4(0);
    }

    /**
     * @dev Get domain separator in scope of EIP-712.
     *
     * @return EIP-712 domain.
     */
    function getDomainSeparator() public virtual view returns(bytes32) {
        return DOMAIN_SEPARATOR;
    }

    /**
     * @dev Enable / disable whitelist protection.
     *
     * @param _status True to disable whitelist, false ot enable.
     */
    function toggleWhitelist(bool _status) external onlyOwner {
        whitelistDisabled = _status;
        emit WhitelistStatusChanged(msg.sender, _status);
    }

    /**
     * @dev Updated validator status.
     *
     * @param _validator Address of validator.
     */
    function addValidator(address _validator) external onlyOwner {
        validators[_validator] = true;
        emit ValidatorAdded(_validator);
    }

    /**
     * @dev Updated validator status.
     *
     * @param _validator Address of validator.
     */
    function removeValidator(address _validator) external onlyOwner {
        validators[_validator] = false;
        emit ValidatorRemoved(_validator);
    }

    /**
     * @dev Check if specific interface is implemented.
     * @param _interfaceID Keccak of matched interface.
     * @return true if interface is implemented.
     */
    function supportsInterface(bytes4 _interfaceID) public override pure returns (bool) {
        return _interfaceID == COMMITMENT_WITH_WHITELIST || super.supportsInterface(_interfaceID);
    }
}