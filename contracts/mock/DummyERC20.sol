// SPDX-License-Identifier: BSD-2-Clause
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract DummyERC20 is ERC20 {
    constructor() ERC20("Test token", "TST") {
    }

    function mint(uint256 _amount) external {
        _mint(msg.sender, _amount);
    }
}