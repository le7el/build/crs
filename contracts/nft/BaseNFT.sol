// SPDX-License-Identifier: BSD-2-Clause
pragma solidity ^0.8.0;

import "../registry/ICRS.sol";
import "../resolver/profile/IRoyaltiesResolver.sol";
import "../resolver/profile/IProxyConfigResolver.sol";
import "../resolver/proxies/ITokenURIProxy.sol";
import "@openzeppelin/contracts/utils/Address.sol";
import "@openzeppelin/contracts/token/ERC721/ERC721.sol";
import "./IBaseNFT.sol";

/** 
 * @dev ERC721 NFT to track ownership of node in CRS registry.
 *      NFT contract is an owner of the relevant level-1 node,
 *      configuration options for that node are exposed through it to the manager. 
 *      Registrations and renewals are delegated to controller contracts.
 */
abstract contract BaseNFT is ERC721, IBaseNFT {
    using Address for address payable;

    // A map of expiry times
    mapping(uint256 => uint) public expiries;
    mapping(uint256 => string) public names;

    bytes4 constant public TOKEN_URI_SELECTOR = bytes4(keccak256("tokenURI(uint256,string,string)"));
    bytes32 constant private ROOT_NODE = 0xd0340a34011af087b374bbdc5136a48a841af1b55b0af1143ced23c89cf182c9; // "l7l"
    bytes4 constant private INTERFACE_META_ID = bytes4(keccak256("supportsInterface(bytes4)"));
    bytes4 constant private ERC721_ID = bytes4(
        keccak256("balanceOf(address)") ^
        keccak256("ownerOf(uint256)") ^
        keccak256("approve(address,uint256)") ^
        keccak256("getApproved(uint256)") ^
        keccak256("setApprovalForAll(address,bool)") ^
        keccak256("isApprovedForAll(address,address)") ^
        keccak256("transferFrom(address,address,uint256)") ^
        keccak256("safeTransferFrom(address,address,uint256)") ^
        keccak256("safeTransferFrom(address,address,uint256,bytes)")
    );
    bytes4 constant private RECLAIM_ID = bytes4(keccak256("reclaim(uint256,address)"));

    /**
     * v2.1.3 version of _isApprovedOrOwner which calls ownerOf(tokenId) and takes grace period into consideration instead of ERC721.ownerOf(tokenId);
     * https://github.com/OpenZeppelin/openzeppelin-contracts/blob/v2.1.3/contracts/token/ERC721/ERC721.sol#L187
     * @dev Returns whether the given spender can transfer a given token ID
     * @param spender address of the spender to query
     * @param tokenId uint256 ID of the token to be transferred
     * @return bool whether the msg.sender is approved for the given token ID,
     *    is an operator of the owner, or is the owner of the token
     */
    function _isApprovedOrOwner(address spender, uint256 tokenId) internal view override returns (bool) {
        address owner = ownerOf(tokenId);
        return (spender == owner || getApproved(tokenId) == spender || isApprovedForAll(owner, spender));
    }

    modifier live {
        require(crs.owner(baseNode) == address(this), "not live");
        _;
    }

    modifier onlyController {
        require(controllers[msg.sender], "not controller");
        _;
    }

    /**
     * @dev Gets the owner of the specified token ID. Names become unowned
     *      when their registration expires.
     * @param tokenId uint256 ID of the token to query the owner of
     * @return address currently marked as the owner of the given token ID
     */
    function ownerOf(uint256 tokenId) public view override(IERC721, ERC721) returns (address) {
        // solhint-disable not-rely-on-time
        require(expiries[tokenId] > block.timestamp, "lease expired");
        return super.ownerOf(tokenId);
    }

    /**
     * @dev Authorises a controller, who can register and renew records.
     * @param _controller Address of controller smart contract which can register NFTs.
     */
    function addController(address _controller) external override onlyOwner {
        controllers[_controller] = true;
        emit ControllerAdded(_controller);
    }

    /**
     * @dev Revoke controller permission for an address.
     * @param _controller Address of controller smart contract which can register NFTs.
     */
    function removeController(address _controller) external override onlyOwner {
        controllers[_controller] = false;
        emit ControllerRemoved(_controller);
    }

    /**
     * @dev Set the resolver for the TLD this registrar manages.
     * @param _resolver Address of resolver smart contract for all NFTs.
     */
    function setResolver(address _resolver) external override onlyOwner {
        crs.setResolver(baseNode, _resolver);
    }

    /**
     * @dev Low-level call by controller to resolver of the baseNode.
     * @param _callData encoded bytestring of the call e.g. `abi.encodeWithSignature("myFunction(uint,address)", 10, msg.sender)`.
     * @return execution result tuple.
     */
    function setBasenodeResolverSettings(bytes calldata _callData) external override onlyOwner returns (bool, bytes memory) {
        // solhint-disable avoid-low-level-calls
        return crs.resolver(baseNode).call(_callData);
    }

    /**
     * @dev Fetch gas fee from resolver of a root node.
     * @param _duration duration in seconds for lease or extension.
     * @return beneficiary address.
     * @return fee in gas token for registration or extenting lease.
     */
    function gasFee(uint256 _duration) public view returns(address, uint256) {
        (address _beneficiary, uint256 _amount,) = IRoyaltiesResolver(crs.resolver(ROOT_NODE)).royalty(ROOT_NODE, address(this));
        return (_beneficiary, _amount * _duration);
    }

    /**
     * @dev Returns the expiration timestamp of the specified id.
     * @return UNIX timestamp for expiration date.
     */
    function nameExpires(uint256 id) external view override returns(uint) {
        return expiries[id];
    }

    /**
     * @dev Returns true if the specified name is available for registration.
     * @return Not available if it's registered here or in its grace period.
     */
    function available(uint256 id) public view override returns(bool) {
        // solhint-disable not-rely-on-time
        return expiries[id] + GRACE_PERIOD < block.timestamp;
    }

    /**
     * @dev Register a name.
     * @param id The token ID (keccak256 of the label).
     * @param owner The address that should own the registration.
     * @param duration Duration in seconds for the registration.
     * @return UNIX timestamp for a new expiration date.
     */
    function register(uint256 id, address owner, uint256 duration) external override payable returns(uint) {
      return _register(id, owner, duration, true);
    }

    /**
     * @dev Register a name, without modifying the registry.
     * @param id The token ID (keccak256 of the label).
     * @param owner The address that should own the registration.
     * @param duration Duration in seconds for the registration.
     * @return UNIX timestamp for a new expiration date.
     */
    function registerOnly(uint256 id, address owner, uint256 duration) external payable returns(uint) {
      return _register(id, owner, duration, false);
    }

    /**
     * @dev Register a name, with possibility to modify the registry.
     * @param _id The token ID (keccak256 of the label).
     * @param _owner The address that should own the registration.
     * @param _duration Duration in seconds for the registration.
     * @param _updateRegistry Modify the registry if true.
     * @return UNIX timestamp for a new expiration date.
     */
    function _register(uint256 _id, address _owner, uint256 _duration, bool _updateRegistry) internal live onlyController returns(uint) {
        require(available(_id), "not available");
        uint256 _expires = block.timestamp + _duration;
        require(_expires + GRACE_PERIOD > block.timestamp + GRACE_PERIOD, "future overflow");
        _maybePayGasFee(_duration);

        expiries[_id] = _expires;
        if (_exists(_id)) {
            // Name was previously owned, and expired
            _burn(_id);
        }
        _mint(_owner, _id);
        if (_updateRegistry) {
            crs.setSubnodeOwner(baseNode, bytes32(_id), _owner);
        }

        emit NameRegistered(_id, _owner, _expires);
        return _expires;
    }

    /**
     * @dev Extend the lease for CRS.
     * @param _id The token ID (keccak256 of the label).
     * @param _duration Duration in seconds for the registration.
     * @return UNIX timestamp for a new expiration date.
     */
    function renew(uint256 _id, uint256 _duration) external override payable live onlyController returns(uint) {
        require(_duration > 0, "invalid duration");
        _maybePayGasFee(_duration);
        
        uint256 _expiries = expiries[_id];
        require(_expiries + GRACE_PERIOD >= block.timestamp, "not registered");
        require(_expiries + _duration + GRACE_PERIOD > _duration + GRACE_PERIOD, "future overflow");

        _expiries += _duration;
        expiries[_id] = _expiries;

        emit NameRenewed(_id, _expiries);
        return _expiries;
    }

    /**
     * @dev Reclaim ownership of a name in CRS, if you own it in the registrar.
     * @param _id The token ID (keccak256 of the label).
     * @param _owner New owner address.
     */
    function reclaim(uint256 _id, address _owner) external override live {
        require(_isApprovedOrOwner(msg.sender, _id), "access denied");
        crs.setSubnodeOwner(baseNode, bytes32(_id), _owner);
    }

    /**
     * @dev NFT metadata.
     * @param _id The token ID (keccak256 of the label).
     * @return Metadata URI or base64 encoded metadata.
     */
    function tokenURI(uint256 _id) public override(ERC721) view virtual returns (string memory) {
        address _proxy = IProxyConfigResolver(crs.resolver(baseNode)).proxyConfig(baseNode, address(this), TOKEN_URI_SELECTOR);
        return ITokenURIProxy(_proxy).tokenURI(_id, names[_id], "");
    }

    /**
     * @dev Save name on-chain for metadata storage.
     * @param _name UTF8 encoded string of domain name.
     */
    function saveName(string memory _name) external override(IBaseNFT) onlyController {
        bytes32 _label = keccak256(bytes(_name));
        uint256 _tokenId = uint256(_label);
        names[_tokenId] = _name;
    }

    /**
     * @dev Automatically try to pay gas fee if needed.
     * @param _duration Seconds of lease.
     */
    function _maybePayGasFee(uint256  _duration) internal {
        (address _beneficiary, uint256 _amount) = gasFee(_duration);
        require(msg.value >= _amount, "not enough coin");
        if (_amount > 0) payable(_beneficiary).sendValue(_amount);
    }

    /**
     * @dev Reclaim ownership of a name in CRS, if you own it in the registrar.
     * @param _interfaceID Bytes4 from keccak256 of interface signature.
     * @return true if interface is supported.
     */
    function supportsInterface(bytes4 _interfaceID) public override(ERC721, IERC165) pure returns (bool) {
        return _interfaceID == INTERFACE_META_ID ||
               _interfaceID == ERC721_ID ||
               _interfaceID == RECLAIM_ID;
    }
}