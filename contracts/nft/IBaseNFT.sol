// SPDX-License-Identifier: BSD-2-Clause
pragma solidity ^0.8.0;

import "../registry/ICRS.sol";
import "@openzeppelin/contracts/token/ERC721/IERC721.sol";
import "@openzeppelin/contracts/access/Ownable.sol";

abstract contract IBaseNFT is Ownable, IERC721 {
    uint constant public GRACE_PERIOD = 90 days;

    event ControllerAdded(address indexed controller);
    event ControllerRemoved(address indexed controller);
    event NameMigrated(uint256 indexed id, address indexed owner, uint expires);
    event NameRegistered(uint256 indexed id, address indexed owner, uint expires);
    event NameRenewed(uint256 indexed id, uint expires);

    /// The CRS registry
    ICRS public crs;

    /// The namehash of the TLD this registrar owns (eg, .eth)
    bytes32 public baseNode;

    /// A map of addresses that are authorised to register and renew names.
    mapping(address=>bool) public controllers;

    /**
     * @dev Authorises a controller, who can register and renew domains.
     */
    function addController(address controller) virtual external;

    /**
     * @dev Revoke controller permission for an address.
     */
    function removeController(address controller) virtual external;

    /**
     * @dev Set the resolver for the TLD this registrar manages.
     */
    function setResolver(address resolver) virtual external;

    /**
     * @dev Returns the expiration timestamp of the specified label hash.
     */
    function nameExpires(uint256 id) virtual external view returns(uint);

    /**
     * @dev Returns true if the specified name is available for registration.
     */
    function available(uint256 id) virtual public view returns(bool);

    /**
     * @dev Register a name.
     */
    function register(uint256 id, address owner, uint duration) virtual external payable returns(uint);

    /**
     * @dev Extend the lease.
     */
    function renew(uint256 id, uint duration) virtual external payable returns(uint);

    /**
     * @dev Reclaim ownership of a name in CRS, if you own it in the registrar.
     */
    function reclaim(uint256 id, address owner) virtual external;

    /**
     * @dev Change baseNode settings on the related resolver.
     */
    function setBasenodeResolverSettings(bytes calldata callData) virtual external returns (bool, bytes memory);

    /**
     * @dev Save name on-chain for metadata storage.
     */
    function saveName(string memory name) virtual external;
}