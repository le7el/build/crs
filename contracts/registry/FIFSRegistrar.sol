// SPDX-License-Identifier: BSD-2-Clause
pragma solidity ^0.8.0;

import "./ICRS.sol";

/**
 * @dev A registrar that allocates subdomains to the first person to claim them.
 */
contract FIFSRegistrar {
    ICRS public crs;
    bytes32 public rootNode;

    modifier only_owner(bytes32 label) {
        address currentOwner = crs.owner(keccak256(abi.encodePacked(rootNode, label)));
        require(currentOwner == address(0) || currentOwner == msg.sender, "Access denied");
        _;
    }

    /**
     * Constructor.
     * @param crsAddr The address of the CRS registry.
     * @param node The node that this registrar administers.
     */
    constructor(ICRS crsAddr, bytes32 node) {
        crs = crsAddr;
        rootNode = node;
    }

    /**
     * Register a name, or change the owner of an existing registration.
     * @param label The hash of the label to register.
     * @param owner The address of the new owner.
     */
    function register(bytes32 label, address owner) public only_owner(label) {
        crs.setSubnodeOwner(rootNode, label, owner);
    }
}
