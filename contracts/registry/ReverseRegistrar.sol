// SPDX-License-Identifier: BSD-2-Clause
pragma solidity ^0.8.0;

import "./ICRS.sol";

abstract contract NameResolver {
    function setName(bytes32 node, string memory name) public virtual;
}

contract ReverseRegistrar {
    // namehash('addr.reverse')
    bytes32 public constant ADDR_REVERSE_NODE = 0x91d1777781884d03a6757a803996e38de2a42967fb37eeaca72729271025a9e2;

    ICRS public crs;
    NameResolver public defaultResolver;

    /**
     * @dev Constructor
     * @param crsAddr The address of the CRS registry.
     * @param resolverAddr The address of the default reverse resolver.
     */
    constructor(ICRS crsAddr, NameResolver resolverAddr) {
        crs = crsAddr;
        defaultResolver = resolverAddr;

        // Assign ownership of the reverse record to our deployer
        ReverseRegistrar oldRegistrar = ReverseRegistrar(crs.owner(ADDR_REVERSE_NODE));
        if (address(oldRegistrar) != address(0)) {
            oldRegistrar.claim(msg.sender);
        }
    }

    /**
     * @dev Transfers ownership of the reverse CRS record associated with the
     *      calling account.
     * @param owner The address to set as the owner of the reverse record in CRS.
     * @return The CRS node hash of the reverse record.
     */
    function claim(address owner) public returns (bytes32) {
        return claimWithResolver(owner, address(0));
    }

    /**
     * @dev Transfers ownership of the reverse CRS record associated with the
     *      calling account.
     * @param owner The address to set as the owner of the reverse record in CRS.
     * @param resolver The address of the resolver to set; 0 to leave unchanged.
     * @return The CRS node hash of the reverse record.
     */
    function claimWithResolver(address owner, address resolver) public returns (bytes32) {
        bytes32 label = sha3HexAddress(msg.sender);
        bytes32 _node = keccak256(abi.encodePacked(ADDR_REVERSE_NODE, label));
        address currentOwner = crs.owner(_node);

        // Update the resolver if required
        if (resolver != address(0) && resolver != crs.resolver(_node)) {
            // Transfer the name to us first if it's not already
            if (currentOwner != address(this)) {
                crs.setSubnodeOwner(ADDR_REVERSE_NODE, label, address(this));
                currentOwner = address(this);
            }
            crs.setResolver(_node, resolver);
        }

        // Update the owner if required
        if (currentOwner != owner) {
            crs.setSubnodeOwner(ADDR_REVERSE_NODE, label, owner);
        }

        return _node;
    }

    /**
     * @dev Sets the `name()` record for the reverse CRS record associated with
     *      the calling account. First updates the resolver to the default reverse
     *      resolver if necessary.
     * @param name The name to set for this address.
     * @return The CRS node hash of the reverse record.
     */
    function setName(string memory name) public returns (bytes32) {
        bytes32 _node = claimWithResolver(address(this), address(defaultResolver));
        defaultResolver.setName(_node, name);
        return _node;
    }

    /**
     * @dev Returns the node hash for a given account's reverse records.
     * @param addr The address to hash
     * @return The CRS node hash.
     */
    function node(address addr) public pure returns (bytes32) {
        return keccak256(abi.encodePacked(ADDR_REVERSE_NODE, sha3HexAddress(addr)));
    }

    /**
     * @dev An optimised function to compute the sha3 of the lower-case
     *      hexadecimal representation of an Ethereum address.
     * @param addr The address to hash
     * @return ret The SHA3 hash of the lower-case hexadecimal encoding of the
     *         input address.
     */
    function sha3HexAddress(address addr) private pure returns (bytes32 ret) {
        addr;
        ret; // Stop warning us about unused variables
        // solhint-disable no-inline-assembly
        assembly {
            let lookup := 0x3031323334353637383961626364656600000000000000000000000000000000

            for { let i := 40 } gt(i, 0) { } {
                i := sub(i, 1)
                mstore8(i, byte(and(addr, 0xf), lookup))
                addr := div(addr, 0x10)
                i := sub(i, 1)
                mstore8(i, byte(and(addr, 0xf), lookup))
                addr := div(addr, 0x10)
            }

            ret := keccak256(0, 40)
        }
    }
}
