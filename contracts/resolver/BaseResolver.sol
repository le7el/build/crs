// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./SupportsInterface.sol";

/**
 * @dev All resolver profiles inherit from BaseResolver to make auth checks.
 */
abstract contract BaseResolver is SupportsInterface {
    /**
     * @dev Checks if caller is either an owner of a node or approved by the owner.
     * @param node 3rd party controller of metadata.
     * @return true if access is authorized.
     */
    function isAuthorised(bytes32 node) internal virtual view returns(bool);

    /**
     * @dev Allow 3rd parties to store isolated records for the node.
     * @param controller 3rd party controller of metadata.
     * @return true if access is authorized.
     */
    function isDelegated(bytes32 node, address controller) internal virtual view returns(bool);

    modifier authorised(bytes32 node) {
        require(isAuthorised(node), "access denied");
        _;
    }

    modifier delegated(bytes32 node, address controller) {
        require(isDelegated(node, controller), "not a delegate");
        _;
    }
}