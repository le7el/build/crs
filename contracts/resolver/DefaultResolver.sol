// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "@openzeppelin/contracts/access/Ownable.sol";

import "../registry/ICRS.sol";
import "./IResolver.sol";
import "./profile/AddressResolver.sol";
import "./profile/ContentHashResolver.sol";
import "./profile/NameResolver.sol";
import "./profile/TextResolver.sol";
import "./profile/InterfaceResolver.sol";
import "./profile/ABIResolver.sol";
import "./profile/PubkeyResolver.sol";
import "./profile/RoyaltiesResolver.sol";
import "./profile/ProxyConfigResolver.sol";
import "./profile/ManagedResolver.sol";
import "./profile/KeyHashResolver.sol";
import "./Multicallable.sol";


/**
 * @dev Default resolver allows owner of a node, configure essential on-chain metadata:
 *      Addresses of wallets on different chains, IPFS and other content hashes, name,
 *      string key-value storage, bytes32 key-value storage, interface and generic proxies,
 *      abi settings, public key, royalty settings, roles and managers etc.
 */
contract DefaultResolver is 
    Multicallable,
    AddressResolver,
    ContentHashResolver,
    NameResolver,
    TextResolver,
    InterfaceResolver,
    ABIResolver,
    PubkeyResolver,
    RoyaltiesResolver,
    ProxyConfigResolver,
    ManagedResolver,
    KeyHashResolver
{
    ICRS public crs;

    /**
     * A mapping of operators. An address that is authorised for an address
     * may make any changes to the name that the owner could, but may not update
     * the set of authorisations.
     * (owner, operator) => approved
     */
    mapping(address => mapping(address => bool)) private _operatorApprovals;

    // Logged when an operator is added or removed.
    event ApprovalForAll(address indexed owner, address indexed operator, bool approved);

    /**
     * @dev Constructs a new resolver.
     * @param _crs Address of CRS registry.
     */
    constructor(ICRS _crs) {
        crs = _crs;
    }

    /**
     * @dev See {IERC1155-setApprovalForAll}.
     */
    function setApprovalForAll(address operator, bool approved) external {
        require(
            msg.sender != operator,
            "ERC1155: can't self-approve"
        );

        _operatorApprovals[msg.sender][operator] = approved;
        emit ApprovalForAll(msg.sender, operator, approved);
    }

    /**
     * @dev Checks if caller is either an owner of a node or approved by the owner.
     * @param node 3rd party controller of metadata.
     * @return true if access is authorized.
     */
    function isAuthorised(bytes32 node) internal override view returns(bool) {
        address owner = crs.owner(node);
        return owner == msg.sender || isApprovedForAll(owner, msg.sender);
    }

    /**
     * @dev Allow 3rd parties to store isolated records for the node.
     * @param controller 3rd party controller of metadata.
     * @return true if access is authorized.
     */
    function isDelegated(bytes32, address controller) internal override view returns(bool) {
        return controller == msg.sender;
    }

    /**
     * @dev See {IERC1155-isApprovedForAll}.
     */
    function isApprovedForAll(address account, address operator) public view returns (bool) {
        return _operatorApprovals[account][operator];
    }

    /**
     * @dev Check if specific interface is implemented.
     * @param interfaceID Keccak of matched interface.
     * @return true if implemented.
     */
    function supportsInterface(bytes4 interfaceID) public override(
      Multicallable,
      AddressResolver,
      ContentHashResolver,
      NameResolver,
      TextResolver,
      InterfaceResolver,
      ABIResolver,
      PubkeyResolver,
      RoyaltiesResolver,
      ProxyConfigResolver,
      ManagedResolver,
      KeyHashResolver
    ) pure returns(bool) {
        return interfaceID == type(IMulticallable).interfaceId || super.supportsInterface(interfaceID);
    }
}