// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

interface IMulticallable {
    /**
     * @dev Execute several resolver calls in a batch.
     * @param data Array of ABI-encoded calls to resolver.
     * @return results array of call results in the same order as input.
     */
    function multicall(bytes[] calldata data) external returns(bytes[] memory results);
}