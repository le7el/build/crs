// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

interface ISupportsInterface {
    /**
     * @dev Check if specific interface is implemented.
     * @param interfaceID Keccak of matched interface.
     * @return true if implemented.
     */
    function supportsInterface(bytes4 interfaceID) external pure returns(bool);
}