// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./IMulticallable.sol";
import "./SupportsInterface.sol";

/**
 * @dev Make it possible to batch several resolver calls into a one transaction.
 */
abstract contract Multicallable is IMulticallable, SupportsInterface {
    /**
     * @dev Execute several resolver calls in a batch.
     * @param data Array of ABI-encoded calls to resolver.
     * @return results array of call results in the same order as input.
     */
    function multicall(bytes[] calldata data) external override returns(bytes[] memory results) {
        results = new bytes[](data.length);
        for(uint i = 0; i < data.length; i++) {
            // solhint-disable avoid-low-level-calls
            (bool success, bytes memory result) = address(this).delegatecall(data[i]);
            require(success, "batched call failed");
            results[i] = result;
        }
        return results;
    }

    /**
     * @dev Check if specific interface is implemented.
     * @param interfaceID Keccak of matched interface.
     * @return true if implemented.
     */
    function supportsInterface(bytes4 interfaceID) public override virtual pure returns(bool) {
        return interfaceID == type(IMulticallable).interfaceId || super.supportsInterface(interfaceID);
    }
}