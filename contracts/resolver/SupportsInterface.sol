// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "./ISupportsInterface.sol";

/**
 * @dev EIP-165 implementation.
 */
abstract contract SupportsInterface is ISupportsInterface {
    /**
     * @dev Check if specific interface is implemented.
     * @param interfaceID Keccak of matched interface.
     * @return true if implemented.
     */
    function supportsInterface(bytes4 interfaceID) virtual override public pure returns(bool) {
        return interfaceID == type(ISupportsInterface).interfaceId;
    }
}