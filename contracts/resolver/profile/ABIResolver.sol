// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../BaseResolver.sol";
import "./IABIResolver.sol";

/**
 * @dev See {https://eips.ethereum.org/EIPS/eip-205}.
 */
abstract contract ABIResolver is IABIResolver, BaseResolver {
    mapping(bytes32=>mapping(uint256=>bytes)) internal abis;

    /**
     * @dev Sets the ABI associated with a CRS node.
     *      Nodes may have one ABI of each content type. To remove an ABI, set it to
     *      the empty string.
     * @param node The node to update.
     * @param contentType The content type of the ABI
     * @param data The ABI data.
     */
    function setABI(bytes32 node, uint256 contentType, bytes calldata data) virtual external authorised(node) {
        // Content types must be powers of 2
        require(((contentType - 1) & contentType) == 0, "invalid bitmask");

        abis[node][contentType] = data;
        emit ABIChanged(node, contentType);
    }

    /**
     * @dev Returns the ABI associated with a CRS node.
     *      Defined in EIP205.
     * @param node The CRS node to query
     * @param contentTypes A bitwise OR of the ABI formats accepted by the caller.
     * @return contentType The content type of the return value
     * @return data The ABI data
     */
    function ABI(bytes32 node, uint256 contentTypes) virtual override external view returns (uint256, bytes memory) {
        mapping(uint256=>bytes) storage abiset = abis[node];

        for (uint256 contentType = 1; contentType <= contentTypes; contentType <<= 1) {
            if ((contentType & contentTypes) != 0 && abiset[contentType].length > 0) {
                return (contentType, abiset[contentType]);
            }
        }

        return (0, bytes(""));
    }

    /**
     * @dev Check if specific interface is implemented.
     * @param interfaceID Keccak of matched interface.
     * @return true if implemented.
     */
    function supportsInterface(bytes4 interfaceID) virtual override public pure returns(bool) {
        return interfaceID == type(IABIResolver).interfaceId || super.supportsInterface(interfaceID);
    }
}