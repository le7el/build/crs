// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../BaseResolver.sol";
import "./IAddressResolver.sol";

/**
 * @dev Human-reable name for wallets on different networks.
 */
abstract contract AddressResolver is IAddressResolver, BaseResolver {
    uint constant private COIN_TYPE_MATIC = 137;

    mapping(bytes32=>mapping(uint=>bytes)) internal _addresses;

    /**
     * @dev Returns the address associated with a CRS node.
     * @param node The CRS node to query.
     * @return The associated address.
     */
    function addr(bytes32 node) virtual public view returns (address payable) {
        bytes memory a = addr(node, COIN_TYPE_MATIC);
        if(a.length == 0) {
            return payable(0);
        }
        return _bytesToAddress(a);
    }
    function addr(bytes32 node, uint coinType) virtual override public view returns(bytes memory) {
        return _addresses[node][coinType];
    }

    /**
     * @dev Sets the address associated with a CRS node.
     *      May only be called by the owner of that node in the CRS registry.
     * @param node The node to update.
     * @param a The address to set.
     */
    function setAddr(bytes32 node, address a) external authorised(node) {
        setAddr(node, COIN_TYPE_MATIC, _addressToBytes(a));
    }

    /**
     * @dev Sets the address associated with a CRS node.
     *      May only be called by the owner of that node in the CRS registry.
     * @param node The node to update.
     * @param coinType Chain id to set address.
     * @param a The address to set.
     */
    function setAddr(bytes32 node, uint coinType, bytes memory a) virtual public authorised(node) {
        emit AddressChanged(node, coinType, a);
        _addresses[node][coinType] = a;
    }

    /**
     * @dev Check if specific interface is implemented.
     * @param interfaceID Keccak of matched interface.
     * @return true if implemented.
     */
    function supportsInterface(bytes4 interfaceID) virtual override public pure returns(bool) {
        return interfaceID == type(IAddressResolver).interfaceId || super.supportsInterface(interfaceID);
    }

    /**
     * @dev Convert from bytes to address type.
     * @param b Address encoded as bytes.
     * @return a decoded address.
     */
    function _bytesToAddress(bytes memory b) internal pure returns(address payable a) {
        require(b.length == 20, "not an address");
        // solhint-disable no-inline-assembly
        assembly {
            a := div(mload(add(b, 32)), exp(256, 12))
        }
    }

    /**
     * @dev Convert from address to bytes type.
     * @param a Address to encode
     * @return b encoded address.
     */
    function _addressToBytes(address a) internal pure returns(bytes memory b) {
        b = new bytes(20);
        // solhint-disable no-inline-assembly
        assembly {
            mstore(add(b, 32), mul(a, exp(256, 12)))
        }
    }
}