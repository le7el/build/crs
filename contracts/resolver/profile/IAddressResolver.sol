// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

/**
 * Interface for addr function.
 */
interface IAddressResolver {
    event AddressChanged(bytes32 indexed node, uint coinType, bytes newAddress);

    /**
     * @dev Returns the address associated with a CRS node.
     * @param node The CRS node to query.
     * @return The associated address.
     */
    function addr(bytes32 node, uint coinType) external view returns(bytes memory);
}