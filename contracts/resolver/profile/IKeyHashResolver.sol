// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

interface IKeyHashResolver {
    event KeyHashChanged(bytes32 indexed node, bytes4 indexed key, bytes32 keyhash);

    /**
     * @dev Returns the hash associated with a CRS node for a key.
     * @param node The CRS node to query.
     * @param key bytes4 signature of a key generated like bytes4(keccak256("KEY")).
     * @return The associated hash.
     */
    function keyHash(bytes32 node, bytes4 key) external view returns (bytes32);
}