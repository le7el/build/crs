// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../ISupportsInterface.sol";
import "./AddressResolver.sol";
import "./IInterfaceResolver.sol";

/**
 * @dev Defines a proxy address which implements specific EIP 165 interface associated with a node.
 */
abstract contract InterfaceResolver is IInterfaceResolver, AddressResolver {
    mapping(bytes32=>mapping(bytes4=>address)) internal interfaces;

    /**
     * @dev Sets an interface associated with a name.
     *      Setting the address to 0 restores the default behaviour of querying the contract at `addr()` for interface support.
     * @param node The node to update.
     * @param interfaceID The EIP 165 interface ID.
     * @param implementer The address of a contract that implements this interface for this node.
     */
    function setInterface(bytes32 node, bytes4 interfaceID, address implementer) virtual external authorised(node) {
        interfaces[node][interfaceID] = implementer;
        emit InterfaceChanged(node, interfaceID, implementer);
    }

    /**
     * @dev Returns the address of a contract that implements the specified interface for this name.
     *      If an implementer has not been set for this interfaceID and name, the resolver will query
     *      the contract at `addr()`. If `addr()` is set, a contract exists at that address, and that
     *      contract implements EIP165 and returns `true` for the specified interfaceID, its address
     *      will be returned.
     * @param node The CRS node to query.
     * @param interfaceID The EIP 165 interface ID to check for.
     * @return The address that implements this interface, or 0 if the interface is unsupported.
     */
    function interfaceImplementer(bytes32 node, bytes4 interfaceID) virtual override external view returns (address) {
        address implementer = interfaces[node][interfaceID];
        if(implementer != address(0)) {
            return implementer;
        }

        address a = addr(node);
        if(a == address(0)) {
            return address(0);
        }

        (bool success, bytes memory returnData) = a.staticcall(abi.encodeWithSignature("supportsInterface(bytes4)", type(ISupportsInterface).interfaceId));
        if(!success || returnData.length < 32 || returnData[31] == 0) {
            // EIP 165 not supported by target
            return address(0);
        }

        (success, returnData) = a.staticcall(abi.encodeWithSignature("supportsInterface(bytes4)", interfaceID));
        if(!success || returnData.length < 32 || returnData[31] == 0) {
            // Specified interface not supported by target
            return address(0);
        }

        return a;
    }

    /**
     * @dev Check if specific interface is implemented.
     * @param interfaceID Keccak of matched interface.
     * @return true if implemented.
     */
    function supportsInterface(bytes4 interfaceID) virtual override public pure returns(bool) {
        return interfaceID == type(IInterfaceResolver).interfaceId || super.supportsInterface(interfaceID);
    }
}