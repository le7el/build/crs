// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../BaseResolver.sol";
import "./IKeyHashResolver.sol";

/**
 * @dev Storage-optimised key-value storage for bytes32 hashes.
 *      The main intended usage is storage of compressed IPFS hashes
 *      associated with different records for a node.
 *      Check `ContentHashResolver` if node needs only one record.
 */
abstract contract KeyHashResolver is IKeyHashResolver, BaseResolver {
    mapping(bytes32=>mapping(bytes4=>bytes32)) internal keyHashes;

    /**
     * @dev Sets the contenthash associated with a CRS node.
     *      May only be called by the owner of that node in the CRS registry.
     * @param _node The node to update.
     * @param _key bytes4 signature of a key generated like bytes4(keccak256("KEY")).
     * @param _keyhash Hash to store, trimmed to bytes32 storage type.
     */
    function setKeyHash(bytes32 _node, bytes4 _key, bytes32 _keyhash) virtual external authorised(_node) {
        keyHashes[_node][_key] = _keyhash;
        emit KeyHashChanged(_node, _key, _keyhash);
    }

    /**
     * @dev Returns the hash associated with a CRS node for a key.
     * @param _node The CRS node to query.
     * @param _key bytes4 signature of a key generated like bytes4(keccak256("KEY")).
     * @return The associated hash.
     */
    function keyHash(bytes32 _node, bytes4 _key) virtual external override view returns (bytes32) {
        return keyHashes[_node][_key];
    }

    /**
     * @dev Check if specific interface is implemented.
     * @param interfaceID Keccak of matched interface.
     * @return true if implemented.
     */
    function supportsInterface(bytes4 interfaceID) virtual override public pure returns(bool) {
        return interfaceID == type(IKeyHashResolver).interfaceId || super.supportsInterface(interfaceID);
    }
}