// SPDX-License-Identifier: MIT
pragma solidity ^0.8.10;

import "../BaseResolver.sol";
import "./IManagedResolver.sol";

/**
 * @dev Define roles or permissions associated with a node.
 */
abstract contract ManagedResolver is IManagedResolver, BaseResolver {
    mapping(bytes32 => mapping(bytes4 => mapping(address => bool))) internal managementRoles; 
    
    /**
     * @dev Assign some role to an address.
     * @param _node the node to update.
     * @param _roleSig bytes4 signature of a role generated like bytes4(keccak256("ROLE_NAME")).
     * @param _manager address which will get the role.
     * @param _active true to set role, false to revoke it.
     */
    function setRole(bytes32 _node, bytes4 _roleSig, address _manager, bool _active) virtual external authorised(_node) {
        managementRoles[_node][_roleSig][_manager] = _active;
        emit RoleChanged(_node, _roleSig, _manager, _active);
    }

    /**
     * @dev Check if manager address has some role.
     * @param _node the node to update.
     * @param _roleSig bytes4 signature of a role generated like bytes4(keccak256("ROLE_NAME")).
     * @param _manager address which will get the role.
     * @return true if manager address has role.
     */
    function hasRole(bytes32 _node, bytes4 _roleSig, address _manager) virtual override external view returns (bool) {
        return managementRoles[_node][_roleSig][_manager];
    }

    /**
     * @dev Check if specific interface is implemented.
     * @param interfaceID Keccak of matched interface.
     * @return true if implemented.
     */
    function supportsInterface(bytes4 interfaceID) virtual override public pure returns(bool) {
        return interfaceID == type(IManagedResolver).interfaceId || super.supportsInterface(interfaceID);
    }
}
