// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../BaseResolver.sol";
import "./IProxyConfigResolver.sol";

/**
 * @dev Allows 3rd parties to associate their own records related to this node.
 *      It can be used to set game profiles, reputation scores or other data, which
 *      shouldn't be controlled by the owner of a node.
 */
abstract contract ProxyConfigResolver is IProxyConfigResolver, BaseResolver {
    mapping(bytes32=>mapping(address=>mapping(bytes4=>address))) internal proxyConfigs;

    /**
     * @dev Sets proxy contract controlled by a 3rd party with data related to this CRS.
     *      Use InterfaceResolver to define proxies for more complex interfaces.
     * @param node The node to update.
     * @param controller Address of proxy controller.
     * @param selector Function selector to be called on proxy contract.
     * @param proxy Address which implements proxy interface.
     */
    function setProxyConfig(
        bytes32 node,
        address controller,
        bytes4 selector,
        address proxy
    ) virtual external delegated(node, controller) {
        proxyConfigs[node][controller][selector] = proxy;
        emit ProxyConfigChanged(node, controller, selector, proxy);
    }

    /**
     * @dev Returns proxy contract address which resolves into some content.
     * @param node The CRS node to query.
     * @param controller Address of proxy controller.
     * @param selector Function selector to be called on proxy contract.
     * @return Address which implements proxy interface.
     */
    function proxyConfig(bytes32 node, address controller, bytes4 selector) virtual override external view returns (address) {
        return proxyConfigs[node][controller][selector];
    }

    /**
     * @dev Check if specific interface is implemented.
     * @param interfaceID Keccak of matched interface.
     * @return true if implemented.
     */
    function supportsInterface(bytes4 interfaceID) virtual override public pure returns(bool) {
        return interfaceID == type(IProxyConfigResolver).interfaceId || super.supportsInterface(interfaceID);
    }
}