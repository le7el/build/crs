// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

import "../BaseResolver.sol";
import "./IRoyaltiesResolver.sol";

/**
 * @dev Defines royalties associated with a node. Supports default and
 *      individual roylties for specific callers. Token or gas coin, amount and
 *      beneficiary are configurable, but interpetation and implementation are
 *      on a side of a caller.
 */
abstract contract RoyaltiesResolver is IRoyaltiesResolver, BaseResolver {
    struct Royalties {
        address beneficiary;
        uint256 amount;
        address token;
    }

    address public constant ZERO_ADDRESS = address(0);

    mapping(bytes32=>Royalties) internal royalties;
    mapping(bytes32=>mapping(address=>Royalties)) internal addressRoyalties;
    
    /**
     * @dev Sets the royalties associated with a CRS node, for reverse records.
     *      May only be called by the owner of that node in the CRS registry.
     * @param node The node to update.
     * @param beneficiary The associated beneficiary to recieve royalties.
     * @param amount The associated royalty amount.
     * @param token ERC20 token to charge royalites, address(0) to use gas coin.
     * @param forAddress Address which individual settings are used, address(0) for default settings.
     */
    function setRoyalties(bytes32 node, address beneficiary, uint256 amount, address token, address forAddress) virtual external authorised(node) {
        if (forAddress == address(0)) {
            royalties[node] = Royalties(beneficiary, amount, token);
        } else {
            addressRoyalties[node][forAddress] = Royalties(beneficiary, amount, token);
        }
        emit RoyaltiesChanged(node, beneficiary, amount, token, forAddress);
    }

    /**
     * @dev Returns the royalties associated with a CRS node.
     * @param node The CRS node to query.
     * @param addr Address for which royalty was set.
     * @return The associated beneficiary to recieve royalties.
     * @return The associated royalty amount.
     * @return The associated royalty token or address(0) for gas coin.
     */
    function royalty(bytes32 node, address addr) virtual override external view returns(address, uint256, address) {
        if (addr == ZERO_ADDRESS) {
            return _defaultRoyalty(node);
        } else {
            address _beneficiary = addressRoyalties[node][addr].beneficiary;
            if (_beneficiary == ZERO_ADDRESS) {
                return _defaultRoyalty(node);
            } else {
                return (_beneficiary, addressRoyalties[node][addr].amount, addressRoyalties[node][addr].token);
            }
        }
    }

    /**
     * @dev Returns default royalties
     * @param node The CRS node to query.
     * @return The associated beneficiary to recieve royalties.
     * @return The associated royalty amount.
     * @return The associated royalty token or address(0) for gas coin.
     */
    function _defaultRoyalty(bytes32 node) internal view returns(address, uint256, address) {
        return (royalties[node].beneficiary, royalties[node].amount, royalties[node].token);
    }

    /**
     * @dev Check if specific interface is implemented.
     * @param interfaceID Keccak of matched interface.
     * @return true if implemented.
     */
    function supportsInterface(bytes4 interfaceID) virtual override public pure returns(bool) {
        return interfaceID == type(IRoyaltiesResolver).interfaceId || super.supportsInterface(interfaceID);
    }
}