// SPDX-License-Identifier: MIT
pragma solidity ^0.8.0;

interface ITokenURIProxy {
    /**
     * @dev Returns proxy contract address which resolves into some content.
     * @param _id The token id.
     * @param _name The domain name.
     * @param _customAvatar URL to custom avatar.
     * @return Metadata URI or base64 encoded metadata.
     */
    function tokenURI(uint256 _id, string memory _name, string memory _customAvatar) external view returns (string memory);
}