// We require the Hardhat Runtime Environment explicitly here. This is optional
// but useful for running the script in a standalone fashion through `node <script>`.
//
// When running the script with `npx hardhat run <script>` you'll find the Hardhat
// Runtime Environment's members available in the global scope.
const { ethers } = require("hardhat");

const DEPLOYER_ADDRESS = "0xeeF30c8B265Ffa552781C1f8a5F2CECff3D3a84B";
const NULL_ADDRESS = "0x0000000000000000000000000000000000000000";
const ZERO_BYTES32 = "0x0000000000000000000000000000000000000000000000000000000000000000";
// namehash of l7l: 0xd0340a34011af087b374bbdc5136a48a841af1b55b0af1143ced23c89cf182c9
// sha3 of l7l: 0x37372c33bd16695ac1a42f79a40d119a436fa3a44dc50a9117f2380fa5d80acc
const ROOT_DOMAIN = "l7l";
const rootNode = getRootNodeFromTLD(ROOT_DOMAIN);
const L7L_MINT_SECOND = "3168873850"

/**
 * Calculate root node hashes given the top level domain(tld)
 *
 * @param {string} tld plain text tld, for example: 'l7l'
 */
function getRootNodeFromTLD(tld) {
  return {
    namehash: ethers.utils.namehash(tld),
    sha3: ethers.utils.keccak256(ethers.utils.toUtf8Bytes(tld))
  };
}

async function main() {
  // Hardhat always runs the compile task when running scripts with its command
  // line interface.
  //
  // If this script is run directly using `node` you may want to call compile
  // manually to make sure everything is compiled
  // await hre.run('compile');

  // We get the contract to deploy
  const CRSRegistryContract = await ethers.getContractFactory("CRSRegistry");
  const CRSRegistry = await CRSRegistryContract.deploy();
  await CRSRegistry.deployed();
  console.info("CRSRegistry deployed to:", CRSRegistry.address);

  // Deploy resolver
  const DefaultResolverContract = await ethers.getContractFactory("DefaultResolver");
  const DefaultResolver = await DefaultResolverContract.deploy(CRSRegistry.address);
  console.info("DefaultResolver deployed to:", DefaultResolver.address);

  // Deploy L7L NFT, controller and price oracle
  const ReferenceImplementationContract = await ethers.getContractFactory("ReferenceImplementation");
  const ReferenceImplementation = await ReferenceImplementationContract.deploy(CRSRegistry.address, rootNode.namehash);
  await ReferenceImplementation.deployed();
  console.info("ReferenceImplementation deployed to:", ReferenceImplementation.address);

  const GatedControllerContract = await ethers.getContractFactory("GatedController");
  const ReferenceController = await GatedControllerContract.deploy(ReferenceImplementation.address);
  console.info("ReferenceController deployed to:", ReferenceController.address);

  await ReferenceImplementation.addController(ReferenceController.address);
  await (await CRSRegistry.setSubnodeOwner(ZERO_BYTES32, rootNode.sha3, DEPLOYER_ADDRESS)).wait();
  await (await CRSRegistry.setResolver(rootNode.namehash, DefaultResolver.address)).wait();
  await DefaultResolver.setRoyalties(rootNode.namehash, ReferenceController.address, L7L_MINT_SECOND, NULL_ADDRESS, NULL_ADDRESS); // 1 gas coin per year, price in seconds
  await CRSRegistry.setSubnodeOwner(ZERO_BYTES32, rootNode.sha3, ReferenceImplementation.address)
  console.debug("Set CRSRegistry controller for .l7l");

  // Verification
  const NETWORK = "live_goerli"
  console.info("To verify your contracts on etherscan run the following commands:")
  console.info(`npx hardhat verify --network ${NETWORK} ${CRSRegistry.address} \\`)
  console.info(`&& npx hardhat verify --network ${NETWORK} ${DefaultResolver.address} ${CRSRegistry.address} \\`)
  console.info(`&& npx hardhat verify --contract contracts/nft/ReferenceImplementation.sol:ReferenceImplementation --network ${NETWORK} ${ReferenceImplementation.address} ${CRSRegistry.address} ${rootNode.namehash} \\`)
  console.info(`&& npx hardhat verify --network ${NETWORK} ${ReferenceController.address} ${ReferenceImplementation.address} \\`)
}

// We recommend this pattern to be able to use async/await everywhere
// and properly handle errors.
main().catch((error) => {
  console.error(error);
  process.exitCode = 1;
});