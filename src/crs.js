import {ethers} from "ethers"
import {registry as crsConfig} from "./artifacts"

const initContract = (contract_address, readOnly = true, provider = null) => {
  if (!ethers.utils.isAddress(contract_address)) return Promise.reject("not a contract address")
  if(!provider) 
    provider = new ethers.providers.Web3Provider(window.ethereum)
  const signer = readOnly ? provider : provider.getSigner()
  return Promise.resolve(new ethers.Contract(contract_address, crsConfig.abi, signer))
}

const refContract = () => "0xBe2F6999D8B87A70b9f522161d0C734150E54df8"

const resolver = (node, contract_address, provider = null) =>
  initContract(contract_address, true, provider)
    .then(contract => contract.resolver(node))

export {
  refContract,
  resolver
}
export default {
  refContract,
  resolver
}