import {ethers} from "ethers"
import {controller, utils, nft, resolver, crs} from './lib'

const {available, rentPrice, makeCommitment, register, getDomainSeparator, whitelistedCommit} = controller
const {erc20Approve, nameToNode, nameToId, nodeToId, idToNode } = utils

const ADDRESS_ZERO = '0x0000000000000000000000000000000000000000'
const PRIV_KEY = '888fa71d782f31e9d1c952ab74d23a0f8f3f4dc189b8165a94810cf62c805af8' // '0x11169009E2E4956205632177ba1d2F2603342D91'

window.nameToNode = nameToNode
window.nameToId = nameToId
window.nodeToId = nodeToId
window.idToNode = idToNode

function debounce(func, timeout = 300) {
    let timer
    return (...args) => {
        clearTimeout(timer)
        timer = setTimeout(() => { func.apply(this, args) }, timeout)
    }
}

const whitelist = async function(controller, address, name) {
  const whitelistMessage = ethers.utils.keccak256(
      ethers.utils.defaultAbiCoder.encode(
          ["bytes32", "address", "string"], [
              await getDomainSeparator(controller),
              address,
              name
          ],
      ),
  )

  const signer = new ethers.Wallet(PRIV_KEY)
  const whitelistSignature = await signer.signMessage(ethers.utils.arrayify(whitelistMessage))
  const sig = ethers.utils.splitSignature(
      ethers.utils.arrayify(whitelistSignature)
  );
  const whitelistSignatureEncoded = ethers.utils.defaultAbiCoder.encode(
      ["uint8", "bytes32", "bytes32"], [sig.v, sig.r, sig.s],
  )
  return whitelistSignatureEncoded
}

document.getElementById('crs_commit').addEventListener('click', () => {
  const controller = document.getElementById('crs_controller').value
  const name = document.getElementById('crs_commit_name').value
  if (name.length <= 2) return

  let registrant;
  const provider = new ethers.providers.Web3Provider(window.ethereum)
  const secret = ethers.utils.randomBytes(32)
  document.getElementById('crs_secret').value = ethers.utils.hexlify(secret)

  provider.listAccounts()
    .then((accounts) => {
      registrant = accounts[0];
      return makeCommitment(name, registrant, secret, null, null, controller)
    })
    .then((commitment) => {
      document.getElementById('crs_reg_name').value = name;
      document.getElementById('crs_commitment').value = commitment;
      return whitelist(controller, registrant, name)
        .then((pass) => {
          return whitelistedCommit(name, commitment, pass, controller)
        })
    })
    .then((tr) => {
      document.getElementById('crs_commit_mining').style.display = 'block';
      return provider.waitForTransaction(tr.hash)
    })
    .then(() => {
      document.getElementById('crs_commit_mining').style.display = 'none';
      const timerNode = document.getElementById('crs_maturation');
      timerNode.parentNode.style.display = 'block';
      let timer;
      timer = setInterval(() => {
        timerNode.innerHTML = parseInt(timerNode.innerHTML) - 1;
        if (timerNode.innerHTML === '0') {
          document.getElementById('crs_commit').disabled = true;
          document.getElementById('crs_register').disabled = false;
          timerNode.parentNode.style.display = 'none';
          clearInterval(timer);
        }
      }, 1000);
    })
})

document.getElementById('crs_register').addEventListener('click', () => {
  const controller = document.getElementById('crs_controller').value
  const name = document.getElementById('crs_commit_name').value
  if (name.length <= 2) return

  const commitment = document.getElementById('crs_commitment').value
  if (!commitment) return

  document.getElementById('crs_done').innerHTML = 'Please, refresh the page to register a new NFT';
  document.getElementById('crs_done').style.display = 'none';

  const secret = document.getElementById('crs_secret').value
  const duration = document.getElementById('crs_duration').value

  const provider = new ethers.providers.Web3Provider(window.ethereum)
  provider.listAccounts()
    .then((accounts) => {
      return rentPrice(name, duration, controller)
        .then((response) => {
          const token = response[0]
          const amount = response[1]
          if (token === ADDRESS_ZERO || response[1].isZero()) {
            return register(name, accounts[0], duration, secret, null, null, controller)
          } else {
            return erc20Approve(token, controller, amount)
              .then(tr => provider.waitForTransaction(tr.hash))
              .then(_ => register(name, accounts[0], duration, secret, null, null, controller))
          }
        })
    })
    .then(() => {
      document.getElementById('crs_done').style.display = 'block';
    })
    .catch((error) => {
      document.getElementById('crs_done').innerHTML = error;
      document.getElementById('crs_done').style.display = 'block';
    })
})

const checkRecord = (event) => {
  const value = event.target.value
  if (!value) {
    document.getElementById('crs_manage_get').disabled = "disabled"
    document.getElementById('crs_manage_set').disabled = "disabled"
    return
  }

  document.getElementById('crs_manage_get').disabled = false
  document.getElementById('crs_manage_set').disabled = false
}
document.getElementById('crs_manage_name').addEventListener('change', debounce(checkRecord))

document.getElementById('crs_manage_set').addEventListener('click', () => {
  const key = document.getElementById('crs_manage_text_key').value
  const value = document.getElementById('crs_manage_text_value').value
  const name = document.getElementById('crs_manage_name').value
  const node = nameToNode(name)
  if (!key || !value || !name) return 

  crs.resolver(node, crs.refContract())
    .then((resolverAddr) => resolver.setText(node, key, value, resolverAddr))
})

document.getElementById('crs_manage_get').addEventListener('click', () => {
  const key = document.getElementById('crs_manage_text_key').value
  const name = document.getElementById('crs_manage_name').value
  const node = nameToNode(name)
  if (!key || !name) return 

  crs.resolver(node, crs.refContract())
    .then((resolverAddr) => resolver.getText(node, key, resolverAddr))
    .then((value) => document.getElementById('crs_manage_text_value').value = value)
})