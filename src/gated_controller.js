import {gatedController as controllerConfig} from "./artifacts"
import {initContract} from "./utils"

const getDomainSeparator = (contract_address, provider = null) =>
  initContract(contract_address, controllerConfig.abi, true, provider)
    .then(contract => contract.getDomainSeparator())

const whitelistedCommit = (name, commitment, pass, contract_address, provider = null) =>
  initContract(contract_address, controllerConfig.abi, false, provider)
    .then(contract => contract['commit(string,bytes32,bytes)'](name, commitment, pass))

export {
  getDomainSeparator,
  whitelistedCommit
}
export default {
  getDomainSeparator,
  whitelistedCommit
}