import {nft as nftConfig} from "./artifacts"
import {initContract, nameToNftId} from "./utils"

const gasFee = (duration, contract_address, provider = null) =>
  initContract(contract_address, nftConfig.abi, true, provider)
    .then(contract => contract.gasFee(duration))

const getName = (id, contract_address, provider = null) =>
  initContract(contract_address, nftConfig.abi, true, provider)
    .then(contract => contract.names(id))

const getNameExpires = (name, contract_address, provider = null) =>
  initContract(contract_address, nftConfig.abi, true, provider)
    .then(contract => contract.nameExpires( nameToNftId(name) ))

const getMetadata = (id, contract_address, provider = null) =>
  initContract(contract_address, nftConfig.abi, true, provider)
    .then(contract => contract.tokenURI(id))
    .then(response => {
      if (response.startsWith('data:application/json;base64,')) {
        const metadata = JSON.parse(atob(response.replace('data:application/json;base64,', '')))
        return metadata
      }
      return null;
    })

const _chunkQueryTransferFilter = (contract, wallet, from_block, to_block, max_blocks_per_query) => {
  if (to_block === 'latest' || from_block > to_block || from_block < 0) {
    return Promise.all([
      contract.queryFilter(contract.filters.Transfer(null, wallet)),
      contract.queryFilter(contract.filters.Transfer(wallet))
    ])
  }

  let all_ins = []
  let all_outs = []

  console.log(from_block, to_block, max_blocks_per_query)
  for(let i = from_block; i < to_block; i += max_blocks_per_query) {
    const _startBlock = i
    const _endBlock = Math.min(to_block, i + max_blocks_per_query - 1)
    const ins_events = contract.queryFilter(contract.filters.Transfer(null, wallet), _startBlock, _endBlock)
    const out_events = contract.queryFilter(contract.filters.Transfer(wallet), _startBlock, _endBlock)
    all_ins.push(ins_events)
    all_outs.push(out_events)
    console.log(all_ins)
  }

  return Promise.all([
    Promise.all(all_ins).then(ins => ins.flat(1)),
    Promise.all(all_outs).then(outs => outs.flat(1))
  ])
}

const getNftsForWallet = (wallet, contract_address, from_block = 0, to_block = 'latest', max_blocks_per_query = 3450, provider = null) =>
  initContract(contract_address, nftConfig.abi, true, provider)
    .then((contract) => {
      return _chunkQueryTransferFilter(contract, wallet, from_block, to_block, max_blocks_per_query)
    })
    .then(([ins, outs]) => {
      const exclude = outs.reduce((acc, event) => {
        acc[event.topics[3]] = event.blockNumber
        return acc
      }, {})

      return ins.filter((event) => {
        const nftId = event.topics[3]
        return !exclude[nftId] || exclude[nftId] < event.blockNumber
      }).map((event) => event.topics[3])
    })
    

export {
  gasFee,
  getName,
  getNameExpires,
  getMetadata,
  getNftsForWallet
}
export default {
  gasFee,
  getName,
  getNameExpires,
  getMetadata,
  getNftsForWallet
}