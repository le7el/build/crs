import {resolver as resolverConfig} from "./artifacts"
import {initContract} from "./utils"

const getText = (node, key, contract_address, provider = null) =>
  initContract(contract_address, resolverConfig.abi, true, provider)
    .then(contract => contract.text(node, key))

const setText = (node, key, value, contract_address, provider = null) =>
  initContract(contract_address, resolverConfig.abi, false, provider)
    .then(contract => contract.setText(node, key, value))

const getProxyConfig = (node, controller, selector, contract_address, provider = null) =>
  initContract(contract_address, resolverConfig.abi, true, provider)
    .then(contract => contract.getProxyConfig(node, controller, selector))

const setProxyConfig = (node, controller, selector, proxy, contract_address, provider = null) =>
  initContract(contract_address, resolverConfig.abi, false, provider)
    .then(contract => contract.setProxyConfig(node, controller, selector, proxy))

export {
  getText,
  setText,
  getProxyConfig,
  setProxyConfig
}
export default {
  getText,
  setText,
  getProxyConfig,
  setProxyConfig
}