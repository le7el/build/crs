import {ethers} from "ethers"

const ERC20_APPROVE_ABI = ["function approve(address _spender, uint256 _value) public returns (bool success)"]

const initContract = (contract_address, abi, readOnly = true, provider = null) => {
  if (!ethers.utils.isAddress(contract_address)) return Promise.reject("not a contract address")
  if(!provider)
    provider = new ethers.providers.Web3Provider(window.ethereum)
  const signer = readOnly ? provider : provider.getSigner()
  return Promise.resolve(new ethers.Contract(contract_address, abi, signer))
}

const getAccounts = (provider = null) => {
  if(!provider) 
    provider = new ethers.providers.Web3Provider(window.ethereum)
  return provider.send("eth_requestAccounts", [])
}

const erc20Approve = (token_address, spender_address, amount, provider = null) => {
  if(!provider) 
    provider = new ethers.providers.Web3Provider(window.ethereum)
  const signer = provider.getSigner()
  const contract = new ethers.Contract(token_address, ERC20_APPROVE_ABI, signer)
  return contract.approve(spender_address, amount)
}

const nameToNode = (name) =>
  ethers.utils.namehash(name)

const nameToId = (name) =>
  nodeToId(nameToNode(name))

const nodeToId = (node) =>
  ethers.BigNumber.from(node).toString()

const idToNode = (id) =>
  ethers.BigNumber.from(id).toHexString()

const nameToNftId = (name) => 
  ethers.BigNumber.from(ethers.utils.keccak256(ethers.utils.toUtf8Bytes(name))).toString()

export {
  initContract,
  getAccounts,
  erc20Approve,
  nameToNode,
  nameToId,
  nodeToId,
  idToNode,
  nameToNftId
}
export default {
  initContract,
  getAccounts,
  erc20Approve,
  nameToNode,
  nameToId,
  nodeToId,
  idToNode,
  nameToNftId
}