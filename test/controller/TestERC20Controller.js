const CRS = artifacts.require('./registry/CRSRegistry');
const DefaultResolver = artifacts.require('./resolvers/DefaultResolver');
const ReferenceImplementation = artifacts.require('./nft/ReferenceImplementation');
const MetadataProxyExample = artifacts.require('./nft/MetadataProxyExample');
const Controller = artifacts.require('./Controller');
const DummyERC20 = artifacts.require('./DummyERC20');
const { evm, exceptions } = require("../test-utils");
const namehash = require('eth-ens-namehash');
const sha3 = require('web3-utils').sha3;
const toBN = require('web3-utils').toBN;

const DAYS = 24 * 60 * 60;
const NULL_ADDRESS = "0x0000000000000000000000000000000000000000"
const DAILY_GAS_COST = toBN('31688738506').mul(toBN(DAYS));

contract('Controller', function (accounts) {
    let crs;
    let resolver;
    let referenceImplementation;
    let controller;
    let dummyToken;

    const secret = "0x0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF";
    const ownerAccount = accounts[0]; // Account that owns the registrar
    const registrantAccount = accounts[1]; // Account that owns test names
    const otherAccount = accounts[2]; // Some random account
    const leaseFee = DAILY_GAS_COST.mul(toBN(28))
    const totalFee = DAILY_GAS_COST.mul(toBN(29))

    before(async () => {
        dummyToken = await DummyERC20.new();
        await dummyToken.mint('100000000000000000000', {from: registrantAccount});
        await dummyToken.mint('100000000000000000000', {from: otherAccount});

        crs = await CRS.new();
        resolver = await DefaultResolver.new(crs.address);

        referenceImplementation = await ReferenceImplementation.new(crs.address, namehash.hash('l7l'), {from: ownerAccount});
        controller = await Controller.new(
            referenceImplementation.address,
            {from: ownerAccount}
        );
        await referenceImplementation.addController(controller.address, {from: ownerAccount});
        
        await dummyToken.approve(controller.address, '1000000000000000000000', {from: registrantAccount});
        await dummyToken.approve(controller.address, '1000000000000000000000', {from: otherAccount});
        await crs.setSubnodeOwner('0x0', sha3('l7l'), ownerAccount);
        await crs.setResolver(namehash.hash('l7l'), resolver.address, {from: ownerAccount});
        await resolver.setRoyalties(namehash.hash('l7l'), controller.address, '31688738506', NULL_ADDRESS, NULL_ADDRESS, {from: ownerAccount});
        await resolver.setRoyalties(namehash.hash('l7l'), controller.address, '1', dummyToken.address, controller.address, {from: ownerAccount});
        await crs.setSubnodeOwner('0x0', sha3('l7l'), referenceImplementation.address);

        metadataProxy = await MetadataProxyExample.new({from: ownerAccount});
        const TOKEN_URI_SELECTOR = await referenceImplementation.TOKEN_URI_SELECTOR();
        const calldata = resolver.contract.methods.setProxyConfig(
            namehash.hash('l7l'),
            referenceImplementation.address,
            TOKEN_URI_SELECTOR,
            metadataProxy.address
        ).encodeABI();
        await referenceImplementation.setBasenodeResolverSettings(calldata, {from: ownerAccount});
    });

    const checkLabels = {
        "testing": true,
        "longname12345678": true,
        "sixsix": true,
        "five5": true,
        "four": true,
        "iii": true,
        "ii": true,
        "i": true,
        "": false,

        // { ni } { hao } { ma } (chinese; simplified)
        "\u4f60\u597d\u5417": true,

        // { ta } { ko } (japanese; hiragana)
        "\u305f\u3053": true,

        // { poop } { poop } { poop } (emoji)
        "\ud83d\udca9\ud83d\udca9\ud83d\udca9": true,

        // { poop } { poop } (emoji)
        "\ud83d\udca9\ud83d\udca9": true,

        // { poop } (emoji)
        "\ud83d\udca9": true
    };

    it('should report label validity', async () => {
        for (const label in checkLabels) {
            assert.equal(await controller.valid(label), checkLabels[label], label);
        }
    });

    it('should report unused names as available', async () => {
        assert.equal(await controller.available(sha3('available')), true);
    });

    it('should permit new registrations', async () => {
        var commitment = await controller.makeCommitment("newname", registrantAccount, secret);
        var tx = await controller.commit(commitment);
        assert.equal(await controller.commitments(commitment), (await web3.eth.getBlock(tx.receipt.blockNumber)).timestamp);
        const gasFee = await referenceImplementation.gasFee(28 * DAYS);
        var balanceBefore = await dummyToken.balanceOf(controller.address);
        await evm.advanceTime((await controller.MIN_COMMITMENT_AGE()).toNumber());
        var tx = await controller.register("newname", registrantAccount, 28 * DAYS, secret, {from: registrantAccount, value: totalFee});
        assert.equal(tx.logs.length, 2);
        assert.equal(tx.logs[0].event, "PaymentReceived");
        assert.equal(tx.logs[0].args.payer, referenceImplementation.address);
        assert.equal(tx.logs[0].args.amount.toString(), gasFee[1].toString());
        assert.equal(tx.logs[1].event, "NameRegistered");
        assert.equal(tx.logs[1].args.name, "newname");
        assert.equal(tx.logs[1].args.owner, registrantAccount);
        assert.equal((await dummyToken.balanceOf(controller.address)) - balanceBefore, 28 * DAYS);
        assert.equal(leaseFee.toString(), gasFee[1].toString());
        assert.equal((await web3.eth.getBalance(gasFee[0])).toString(), gasFee[1].toString());
    });

    it('should report registered names as unavailable', async () => {
        assert.equal(await controller.available('newname'), false);
    });

    it('should permit new registrations with config', async () => {
        var commitment = await controller.makeCommitmentWithConfig("newconfigname", registrantAccount, secret, resolver.address, registrantAccount);
        var tx = await controller.commit(commitment);
        assert.equal(await controller.commitments(commitment), (await web3.eth.getBlock(tx.receipt.blockNumber)).timestamp);

        const gasFee = await referenceImplementation.gasFee(28 * DAYS);
        var balanceFeeBefore = await web3.eth.getBalance(gasFee[0]);
        var balanceBefore = await dummyToken.balanceOf(controller.address);
        await evm.advanceTime((await controller.MIN_COMMITMENT_AGE()).toNumber());
        var tx = await controller.registerWithConfig("newconfigname", registrantAccount, 28 * DAYS, secret, resolver.address, registrantAccount, {from: registrantAccount, value: totalFee});
        assert.equal(tx.logs.length, 2);
        assert.equal(tx.logs[0].event, "PaymentReceived");
        assert.equal(tx.logs[0].args.payer, referenceImplementation.address);
        assert.equal(tx.logs[0].args.amount.toString(), gasFee[1].toString());
        assert.equal(tx.logs[1].event, "NameRegistered");
        assert.equal(tx.logs[1].args.name, "newconfigname");
        assert.equal(tx.logs[1].args.owner, registrantAccount);
        assert.equal((await dummyToken.balanceOf(controller.address)) - balanceBefore, 28 * DAYS);
        var balanceFeeAfter = await web3.eth.getBalance(gasFee[0]);
        assert.equal(toBN(balanceFeeAfter).sub(toBN(balanceFeeBefore)).toString(), gasFee[1].toString());

        var nodehash = namehash.hash("newconfigname.l7l");
        assert.equal((await crs.resolver(nodehash)), resolver.address);
        assert.equal((await crs.owner(nodehash)), registrantAccount);
        assert.equal((await resolver.addr(nodehash)), registrantAccount);
    });

    it('should not allow a commitment with addr but not resolver', async () => {
        await exceptions.expectFailure(controller.makeCommitmentWithConfig("newconfigname2", registrantAccount, secret, NULL_ADDRESS, registrantAccount));
    });

    it('should permit a registration with resolver but not addr', async () => {
        var commitment = await controller.makeCommitmentWithConfig("newconfigname2", registrantAccount, secret, resolver.address, NULL_ADDRESS);
        var tx = await controller.commit(commitment);
        assert.equal(await controller.commitments(commitment), (await web3.eth.getBlock(tx.receipt.blockNumber)).timestamp);

        const gasFee = await referenceImplementation.gasFee(28 * DAYS);
        var balanceFeeBefore = await web3.eth.getBalance(gasFee[0]);
        var balanceBefore = await dummyToken.balanceOf(controller.address);
        await evm.advanceTime((await controller.MIN_COMMITMENT_AGE()).toNumber());
        var tx = await controller.registerWithConfig("newconfigname2", registrantAccount, 28 * DAYS, secret, resolver.address, NULL_ADDRESS, {from: registrantAccount, value: totalFee});
        assert.equal(tx.logs.length, 2);
        assert.equal(tx.logs[0].event, "PaymentReceived");
        assert.equal(tx.logs[0].args.payer, referenceImplementation.address);
        assert.equal(tx.logs[0].args.amount.toString(), gasFee[1].toString());
        assert.equal(tx.logs[1].event, "NameRegistered");
        assert.equal(tx.logs[1].args.name, "newconfigname2");
        assert.equal(tx.logs[1].args.owner, registrantAccount);
        assert.equal((await dummyToken.balanceOf(controller.address)) - balanceBefore, 28 * DAYS);
        var balanceFeeAfter = await web3.eth.getBalance(gasFee[0]);
        assert.equal(toBN(balanceFeeAfter).sub(toBN(balanceFeeBefore)).toString(), gasFee[1].toString());

        var nodehash = namehash.hash("newconfigname2.l7l");
        assert.equal((await crs.resolver(nodehash)), resolver.address);
        assert.equal((await resolver.addr(nodehash)), 0);
    });

    it('should include the owner in the commitment', async () => {
        await controller.commit(await controller.makeCommitment("newname2", accounts[2], secret));

        await evm.advanceTime((await controller.MIN_COMMITMENT_AGE()).toNumber());
        await exceptions.expectFailure(controller.register("newname2", registrantAccount, 28 * DAYS, secret, {from: registrantAccount, value: totalFee}));
    });

    it('should reject duplicate registrations', async () => {
        await controller.commit(await controller.makeCommitment("newname", registrantAccount, secret));

        await evm.advanceTime((await controller.MIN_COMMITMENT_AGE()).toNumber());
        await exceptions.expectFailure(controller.register("newname", registrantAccount, 28 * DAYS, secret, {from: registrantAccount, value: totalFee}));
    });

    it('should reject for expired commitments', async () => {
        await controller.commit(await controller.makeCommitment("newname2", registrantAccount, secret));

        await evm.advanceTime((await controller.MAX_COMMITMENT_AGE()).toNumber() + 1);
        await exceptions.expectFailure(controller.register("newname2", registrantAccount, 28 * DAYS, secret, {from: registrantAccount, value: totalFee}));
    });

    it('should allow anyone to renew a name', async () => {
        var expires = await referenceImplementation.nameExpires(sha3("newname"));
        await dummyToken.mint(86400);
        await dummyToken.approve(controller.address, 86400);
        var balanceBefore = await dummyToken.balanceOf(controller.address);
        await controller.renew("newname", 86400, {value: totalFee});
        var newExpires = await referenceImplementation.nameExpires(sha3("newname"));
        assert.equal(newExpires.toNumber() - expires.toNumber(), 86400);
        assert.equal((await dummyToken.balanceOf(controller.address)) - balanceBefore, 86400);
    });

    it('should allow registration of expired name', async () => {
        var commitment = await controller.makeCommitment("expiredname", registrantAccount, secret);
        await controller.commit(commitment);
        await evm.advanceTime((await controller.MIN_COMMITMENT_AGE()).toNumber());
        await controller.register("expiredname", registrantAccount, 28 * DAYS, secret, {from: registrantAccount, value: totalFee});
        assert.equal(await controller.available("expiredname"), false);
        await evm.advanceTime(120 * DAYS);
        assert.equal(await controller.available("expiredname"), true);
        
        commitment = await controller.makeCommitment("expiredname", otherAccount, secret);
        await controller.commit(commitment);
        await evm.advanceTime((await controller.MIN_COMMITMENT_AGE()).toNumber());
        const tx = await controller.register("expiredname", otherAccount, 28 * DAYS, secret, {from: otherAccount, value: totalFee});
        assert.equal(tx.logs[1].event, "NameRegistered");
        assert.equal(tx.logs[1].args.name, "expiredname");
        assert.equal(tx.logs[1].args.owner, otherAccount);
    });

    it('should require sufficient token value for a renewal', async () => {
        await dummyToken.approve(controller.address, 86400 * 5); // 4 letter name
        await exceptions.expectError(controller.renew("name", 86400, {value: totalFee}), "ERC20: transfer amount exceeds balance");
    });

    it('should require sufficient gas coin for a renewal', async () => {
        await dummyToken.mint(86400);
        await dummyToken.approve(controller.address, 86400);
        await exceptions.expectError(controller.renew("newname", 86400), "not enough coin");
    });

    it('should allow the registrar owner to withdraw funds', async () => {
        await controller.withdraw(dummyToken.address, {from: ownerAccount});
        assert.equal(await dummyToken.balanceOf(controller.address), 0);
        await controller.withdraw(NULL_ADDRESS, {from: ownerAccount});
        assert.equal(await web3.eth.getBalance(controller.address), 0);
    });

    it('should allow owner to change max possible lease period', async () => {
        await controller.adminMaxLeasePeriod(31536000, {from: ownerAccount});
        assert.equal(await controller.maxLeasePeriod(), 31536000);

        var commitment = await controller.makeCommitment("leasenewname", registrantAccount, secret);
        await controller.commit(commitment);
        await evm.advanceTime((await controller.MIN_COMMITMENT_AGE()).toNumber());
        await exceptions.expectError(
            controller.register("leasenewname", registrantAccount, 31536001, secret, {from: registrantAccount, value: DAILY_GAS_COST.mul(toBN(366))}),
            "too long lease"
        )

        await controller.register("leasenewname", registrantAccount, 31535000, secret, {from: registrantAccount, value: DAILY_GAS_COST.mul(toBN(365))})
        await exceptions.expectError(
            controller.renew("leasenewname", 86400, {value: totalFee}),
            "too long lease"
        )
        await controller.renew("leasenewname", 1000, {value: totalFee})
    });
});
