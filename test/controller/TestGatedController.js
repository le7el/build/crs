const CRS = artifacts.require('./registry/CRSRegistry');
const DefaultResolver = artifacts.require('./resolvers/DefaultResolver');
const ReferenceImplementation = artifacts.require('./nft/ReferenceImplementation');
const Controller = artifacts.require('./GatedController');
const DummyERC20 = artifacts.require('./DummyERC20');
const namehash = require('eth-ens-namehash');
const { whitelisting, evm, exceptions } = require("../test-utils");
const sha3 = require('web3-utils').sha3;
const toBN = require('web3-utils').toBN;

const DAYS = 24 * 60 * 60;
const NULL_ADDRESS = "0x0000000000000000000000000000000000000000"
const DAILY_GAS_COST = toBN('31688738506').mul(toBN(DAYS));
const WRONG_WHITELIST_SIGNATURE = "0x000000000000000000000000000000000000000000000000000000000000001b20dbf31fc19576bd59078b5e0e0ff10d0a6a6d1d3d5358db07d1beaa3ede9f071b0446e8def440bae97587ea7b21956adce69d8c87e1a8e7eb02b34b967829aa"

contract('Controller', function (accounts) {
    let crs;
    let resolver;
    let referenceImplementation;
    let controller;
    let dummyToken;
    let whitelistDomain;

    function whitelistNameForAccount(address, name) {
        return whitelisting.permit(
            "0x5de4111afa1a4b94908f83103eb1f1706367c2e68ca870fc3fb9a804cdab365a", // accounts[2]
            whitelistDomain,
            address,
            name
        )
    }

    const secret = "0x0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF0123456789ABCDEF";
    const ownerAccount = accounts[0]; // Account that owns the registrar
    const registrantAccount = accounts[1]; // Account that owns test names
    const validatorAccount = accounts[2]; // Account that manages whitelist
    const totalFee = DAILY_GAS_COST.mul(toBN(29))

    before(async () => {
        dummyToken = await DummyERC20.new();
        await dummyToken.mint('100000000000000000000', {from: registrantAccount});

        crs = await CRS.new();
        resolver = await DefaultResolver.new(crs.address);

        referenceImplementation = await ReferenceImplementation.new(crs.address, namehash.hash('l7l'), {from: ownerAccount});
        controller = await Controller.new(
            referenceImplementation.address,
            {from: ownerAccount}
        );
        whitelistDomain = await controller.getDomainSeparator();
        await referenceImplementation.addController(controller.address, {from: ownerAccount});
        
        await dummyToken.approve(controller.address, '100000000000000000000', {from: registrantAccount});
        await crs.setSubnodeOwner('0x0', sha3('l7l'), ownerAccount);
        await crs.setResolver(namehash.hash('l7l'), resolver.address, {from: ownerAccount});
        await resolver.setRoyalties(namehash.hash('l7l'), controller.address, '31688738506', NULL_ADDRESS, NULL_ADDRESS, {from: ownerAccount});
        await resolver.setRoyalties(namehash.hash('l7l'), controller.address, '1', dummyToken.address, controller.address, {from: ownerAccount});
        await crs.setSubnodeOwner('0x0', sha3('l7l'), referenceImplementation.address);
    });

    it('should not permit new commitments without whitelisting', async () => {
        var commitment = await controller.makeCommitment("newname", registrantAccount, secret);
        await exceptions.expectError(controller.commit("newname", commitment, WRONG_WHITELIST_SIGNATURE, {from: registrantAccount}), "not whitelisted")
    });

    it('should not permit new commitments approved by invalid validator', async () => {
        var commitment = await controller.makeCommitment("newname", registrantAccount, secret);
        const pass = await whitelistNameForAccount(registrantAccount, "newname");
        await exceptions.expectError(controller.commit("newname", commitment, pass, {from: registrantAccount}), "not whitelisted")
    });

    it('should not permit new commitments with invalid name whitelisting', async () => {
        var commitment = await controller.makeCommitment("newname", registrantAccount, secret);
        await controller.addValidator(validatorAccount);
        const pass = await whitelistNameForAccount(registrantAccount, "newname1");
        await exceptions.expectError(controller.commit("newname", commitment, pass, {from: registrantAccount}), "not whitelisted")
    });

    it('should not permit new commitments approved by outdated validator', async () => {
        var commitment = await controller.makeCommitment("newname", registrantAccount, secret);
        await controller.removeValidator(validatorAccount);
        const pass = await whitelistNameForAccount(registrantAccount, "newname");
        await exceptions.expectError(controller.commit("newname", commitment, pass, {from: registrantAccount}), "not whitelisted")
    });

    it('should not permit new commitments without validation while whitelisting is enabled', async () => {
        var commitment = await controller.makeCommitment("newname", registrantAccount, secret);
        await exceptions.expectError(controller.commit(commitment, "use with whitelisting params"))
    });

    it('should permit new commitments without validation when whitelisting is disabled', async () => {
        await controller.toggleWhitelist(true);
        var commitment = await controller.makeCommitment("newname_wl", registrantAccount, secret);
        var tx = await controller.commit(commitment);
        assert.equal(await controller.commitments(commitment), (await web3.eth.getBlock(tx.receipt.blockNumber)).timestamp);
        const gasFee = await referenceImplementation.gasFee(28 * DAYS);
        await evm.advanceTime((await controller.MIN_COMMITMENT_AGE()).toNumber());
        var tx = await controller.register("newname_wl", registrantAccount, 28 * DAYS, secret, {from: registrantAccount, value: totalFee});
        assert.equal(tx.logs.length, 2);
        assert.equal(tx.logs[0].event, "PaymentReceived");
        assert.equal(tx.logs[0].args.payer, referenceImplementation.address);
        assert.equal(tx.logs[0].args.amount.toString(), gasFee[1].toString());
        assert.equal(tx.logs[1].event, "NameRegistered");
        assert.equal(tx.logs[1].args.name, "newname_wl");
        assert.equal(tx.logs[1].args.owner, registrantAccount);
    });

    it('should permit new registrations with whitelisting', async () => {
        var commitment = await controller.makeCommitment("newname", registrantAccount, secret);
        await controller.addValidator(validatorAccount);
        const pass = await whitelistNameForAccount(registrantAccount, "newname");
        var tx = await controller.commit("newname", commitment, pass, {from: registrantAccount});
        assert.equal(await controller.commitments(commitment), (await web3.eth.getBlock(tx.receipt.blockNumber)).timestamp);
        const gasFee = await referenceImplementation.gasFee(28 * DAYS);
        await evm.advanceTime((await controller.MIN_COMMITMENT_AGE()).toNumber());
        var tx = await controller.register("newname", registrantAccount, 28 * DAYS, secret, {from: registrantAccount, value: totalFee});
        assert.equal(tx.logs.length, 2);
        assert.equal(tx.logs[0].event, "PaymentReceived");
        assert.equal(tx.logs[0].args.payer, referenceImplementation.address);
        assert.equal(tx.logs[0].args.amount.toString(), gasFee[1].toString());
        assert.equal(tx.logs[1].event, "NameRegistered");
        assert.equal(tx.logs[1].args.name, "newname");
        assert.equal(tx.logs[1].args.owner, registrantAccount);
    });
});