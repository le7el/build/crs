const ethers = require('ethers')

const permit = async function(privKey, contractDomain, address, name) {
    const whitelistMessage = ethers.utils.keccak256(
        ethers.utils.defaultAbiCoder.encode(
            ["bytes32", "address", "string"], [
                contractDomain,
                address,
                name
            ],
        ),
    )

    const signer = new ethers.Wallet(privKey)
    const whitelistSignature = await signer.signMessage(ethers.utils.arrayify(whitelistMessage))
    const sig = ethers.utils.splitSignature(
        ethers.utils.arrayify(whitelistSignature)
    );
    const whitelistSignatureEncoded = ethers.utils.defaultAbiCoder.encode(
        ["uint8", "bytes32", "bytes32"], [sig.v, sig.r, sig.s],
    )
    return whitelistSignatureEncoded
}

module.exports = {
  permit
}