module.exports = {
  networks: {
    develop: {
      hdPath: "m/44'/60'/0'/0/",
      mnemonic: 'test test test test test test test test test test test junk',
      host: "127.0.0.1",
      port: 8545,
      network_id: 31337,
      gasPrice: 33000000000,
      gas: 6721975,
      accounts: {
        mnemonic: 'test test test test test test test test test test test junk',
        path: "m/44'/60'/0'/0/"
      }
    }
  },
  compilers: {
    solc: {
      version: "0.8.10",
      settings: {
        optimizer: {
          enabled: true,
          runs: 200
        }
      }
    }
  }
};
