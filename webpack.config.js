const path = require('path');

module.exports = (env, argv) => { 
  return {
    mode: argv.mode === 'production' ? 'production' : 'development',
    devtool: 'inline-source-map',
    devServer: {
      static: './dist'
    },
    entry: {
      example: './src/example.js',
      web3_crs: './src/lib.js',
    },
    output: {
      path: path.resolve(__dirname, 'dist'),
      filename: '[name].js',
      library: {
        name: 'web3_crs',
        type: 'umd',
      }
    }
  }
}